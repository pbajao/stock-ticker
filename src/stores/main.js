import { defineStore } from "pinia";
import { watch } from "vue";
import { useLocalStorage } from "@vueuse/core";
import { Capacitor } from "@capacitor/core";
import { SecureStoragePlugin } from "capacitor-secure-storage-plugin";
import { Preferences } from "@capacitor/preferences";

async function loadPreference(
  store,
  key,
  deserializeFunc = (value) => value,
  serializeFunc = (value) => value
) {
  const { value } = await Preferences.get({ key: key });

  if (value !== null) {
    const deserializedValue = deserializeFunc(value);

    store[key] = deserializedValue;
  }

  watch(
    () => store[key],
    () => {
      Preferences.set({ key: key, value: serializeFunc(store[key]) });
    },
    { deep: true }
  );
}

function encrypt(value) {
  if (window.electronAPI !== undefined) {
    return window.electronAPI.encrypt(value);
  } else {
    return value;
  }
}

function decrypt(encryptedValue) {
  if (window.electronAPI !== undefined) {
    return window.electronAPI.decrypt(encryptedValue);
  } else {
    return encryptedValue;
  }
}

export const useStore = defineStore("main", {
  state: () => ({
    body: null,
    encryptedAlpacaKeyId: null,
    encryptedAlpacaSecretKey: null,
    alpacaLive: null,
    symbols: [],
    symbolsPrice: useLocalStorage("symbolsPrice/v1", {}),
  }),
  getters: {
    isAlpacaLive: (state) => state.alpacaLive == "yes",
  },
  actions: {
    async loadPreferences() {
      await loadPreference(this, "encryptedAlpacaKeyId");
      await loadPreference(this, "encryptedAlpacaSecretKey");
      await loadPreference(this, "alpacaLive");
      await loadPreference(this, "symbols", JSON.parse, JSON.stringify);
    },
    async getAlpacaKeyId() {
      if (Capacitor.isNative) {
        const { value } = await SecureStoragePlugin.get({ key: "alpacaKeyId" });

        return value;
      } else {
        if (this.encryptedAlpacaKeyId !== null) {
          return await decrypt(this.encryptedAlpacaKeyId);
        }
      }
    },
    async getAlpacaSecretKey() {
      if (Capacitor.isNative) {
        const { value } = await SecureStoragePlugin.get({
          key: "alpacaSecretKey",
        });

        return value;
      } else {
        if (this.encryptedAlpacaSecretKey) {
          return await decrypt(this.encryptedAlpacaSecretKey);
        }
      }
    },
    async getAlpacaCredentials() {
      const keyId = await this.getAlpacaKeyId();
      const secretKey = await this.getAlpacaSecretKey();

      if (keyId !== undefined && secretKey !== undefined) {
        return { keyId, secretKey };
      } else {
        return undefined;
      }
    },
    async setEncryptedAlpacaKeyId(keyId) {
      if (keyId !== null) {
        if (Capacitor.isNative) {
          SecureStoragePlugin.set({ key: "alpacaKeyId", value: keyId });
        } else {
          this.encryptedAlpacaKeyId = await encrypt(keyId);
        }
      }
    },
    async setEncryptedAlpacaSecretKey(secretKey) {
      if (secretKey !== null) {
        if (Capacitor.isNative) {
          SecureStoragePlugin.set({ key: "alpacaSecretKey", value: secretKey });
        } else {
          this.encryptedAlpacaSecretKey = await encrypt(secretKey);
        }
      }
    },
    addSymbol(symbol) {
      if (this.symbols.length < 30) {
        this.symbols = [...this.symbols, symbol];
      }
    },
    deleteSymbol(index, symbol) {
      this.symbols.splice(index, 1);
      delete this.symbolsPrice[symbol];
    },
    updateSymbolPrice(symbol, priceInfo) {
      let storedSymbolPrice = this.symbolsPrice[symbol];

      if (storedSymbolPrice !== undefined) {
        if (
          storedSymbolPrice.timestamp === undefined ||
          storedSymbolPrice.timestamp < priceInfo.timestamp
        ) {
          storedSymbolPrice.currentPrice = priceInfo.currentPrice;
          storedSymbolPrice.timestamp = priceInfo.timestamp;
        }

        if (priceInfo.previousClosePrice !== undefined) {
          storedSymbolPrice.previousClosePrice = priceInfo.previousClosePrice;
        }
      } else {
        this.symbolsPrice[symbol] = {
          currentPrice: priceInfo.currentPrice,
          previousClosePrice: priceInfo.previousClosePrice,
          timestamp: priceInfo.timestamp,
        };
      }
    },
  },
});
