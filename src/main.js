import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";
import { useStore } from "@/stores/main";

import "./assets/main.css";

const app = createApp(App);

app.use(createPinia());

const store = useStore();

store.loadPreferences().then(() => {
  app.mount("#app");
});
