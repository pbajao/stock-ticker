import axios from "axios";

const getEndpoint = function (store) {
  return store.isAlpacaLive
    ? "https://api.alpaca.markets"
    : "https://paper-api.alpaca.markets";
};

export const getAsset = async function (store, symbol) {
  const creds = await store.getAlpacaCredentials();

  if (creds !== undefined) {
    return await axios.get(
      `${getEndpoint(store)}/v2/assets/${symbol.toUpperCase()}`,
      {
        headers: {
          "APCA-API-KEY-ID": creds.keyId,
          "APCA-API-SECRET-KEY": creds.secretKey,
        },
      }
    );
  }
};

export const getSnapshot = async function (store, symbol) {
  const creds = await store.getAlpacaCredentials();

  if (creds !== undefined) {
    return await axios.get(
      `https://data.alpaca.markets/v2/stocks/${symbol.toUpperCase()}/snapshot`,
      {
        headers: {
          "APCA-API-KEY-ID": creds.keyId,
          "APCA-API-SECRET-KEY": creds.secretKey,
        },
      }
    );
  }
};

export const getSnapshots = async function (store, symbols) {
  const creds = await store.getAlpacaCredentials();

  if (creds !== undefined) {
    return await axios.get(`https://data.alpaca.markets/v2/stocks/snapshots`, {
      params: {
        symbols: symbols.join(),
      },
      headers: {
        "APCA-API-KEY-ID": creds.keyId,
        "APCA-API-SECRET-KEY": creds.secretKey,
      },
    });
  }
};
