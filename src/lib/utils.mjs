import { useStore } from "@/stores/main";

const getBody = function () {
  const store = useStore();

  return store.body !== undefined ? store.body : document.body;
};

export const disableScroll = function () {
  getBody().style.overflow = "hidden";
};

export const enableScroll = function () {
  getBody().style.overflow = null;
};

export const datesAreOnSameDay = function (first, second) {
  return (
    first.getFullYear() === second.getFullYear() &&
    first.getMonth() === second.getMonth() &&
    first.getDate() === second.getDate()
  );
};
