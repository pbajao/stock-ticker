const { app, BrowserWindow, ipcMain, safeStorage } = require("electron");
const path = require("path");

function createWindow() {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    minWidth: 470,
    minHeight: 300,
    icon: path.join(__dirname, "/public/icon.png"),
    webPreferences: {
      preload: path.join(__dirname, "preload.js"),
    },
  });

  if (process.env.VITE_DEV_SERVER_URL) {
    win.loadURL(process.env.VITE_DEV_SERVER_URL);
  } else {
    win.loadFile("dist/index.html");
  }
}

async function encrypt(event, value) {
  const result = await safeStorage.encryptString(value);
  return result.toString("base64");
}

async function decrypt(event, encryptedValue) {
  const result = await safeStorage.decryptString(
    Buffer.from(encryptedValue, "base64")
  );
  return result;
}

app.whenReady().then(() => {
  ipcMain.handle("encrypt", encrypt);
  ipcMain.handle("decrypt", decrypt);

  createWindow();

  app.on("activate", () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow();
    }
  });
});

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});
