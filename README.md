# Stock Ticker

## Requirements

- [Node.js](https://nodejs.org/)
- [NVM](https://github.com/nvm-sh/nvm) for development
- [Capacitor](https://capacitorjs.com/) [requirements](https://capacitorjs.com/docs/getting-started/environment-setup)
  for developing and building Android and iOS app versions
- [cordova-res](https://github.com/ionic-team/capacitor-assets) for generating app icons and splash screen

## Development

### Setup

1. Install dependencies: `npm install`.
1. Run `npm run dev`. Electron application will open.
1. Add `DEVELOPMENT_TEAM=<your development team ID>` to `ios/App/default.xcconfig`. This is gitignored.

### Run app on Android or iOS simulators

Android:

```sh
npm run build && npx cap sync && npx cap run android
```

iOS:

```sh
npm run build && npx cap sync && npx cap run ios
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

## Packaging and Distribution

### Electron

[electron-forge](https://electronforge.io/) is used to package and distribute
the Electron application.

Run `npm run electron:make` to build application and installers. The results
will be in `out/` directory.

## Icons

- [Favicon](https://www.svgrepo.com/svg/213572/line-chart-graph) and other
  [icons](https://www.svgrepo.com/collection/essential-compilation/1) are from
  [SVG Repo](https://www.svgrepo.com/)
- Loading icon is from [loading.io](https://loading.io/).
- SVG sprite is generated using [svgsprit.es](https://svgsprit.es/)
- Favicon is generated using [Favicon.ico & App Icon Generator](https://www.favicon-generator.org/)
